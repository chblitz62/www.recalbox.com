+++
date = "2018-03-16T01:00:00+02:00"
image = "/images/blog/2018-03-16-recalbox-18-03-16/recalbox-18.03.16-banner.jpg"
title = "Recalbox 18.03.16"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Olá companheiros retrogamers !

Eu espero que vocês tenham curtido nosso lançamento passado e se divertido jogando alguns jogos de Amiga novamente no seu Pi!

O verão Recalbox está chegando um pouco mais cedo este ano com algumas funcionalidades que vocês não irão querer perder! Aqui vai a lista de mudanças:

* ES: novo modo carrosel e novas opções de temas! Acesse Opções da Interface > Configuração de Temas com o novo tema recalbox-next

* ES: não haverá mais a tela branca da morte ao lotar a VRAM! Criadores de temas poderão finalmente adicionar imagens de fundo em 1080p

* segacd: adicionado chd como formato de arquivo suportado

* som: alterar volume apenas para dispositivos de playback. Isto irá ajudar a maioria dos usuários x86 que possuem feedback loop no primeiro boot.

* psx: corrigido erro de grafia (mu3 ao invés de m3u na extensão de arquivo)

* Moonlight: atualizado para 2.4.6, suportando GFE versão 3.12 ou superior + suporte a diversos PCs com GFE + scrape reescrito. Yeah, finalmente você pode atualizar o GFE no seu PC! E a busca de dados do scraping é um misto de informações de jogos do TGDB + capas do GFE. Significa que você pode configurar a capa do seu jogo no GFE

* recalbox.conf videomode agora suporta o novo modo `auto` que facilitará pessoas com telas especiais. Como funciona: O Recalbox irá detectar se a tela pode lidar com 720p. Se sim, troca para 720p. Se não, continua utilizando a resolução padrão. Esta configuração não será padrão por enquanto

* Pi: Desabilitado áudio forçado pela saída HDMI, sendo autodetectado a partir de agora. É uma ótima combinação para o modo auto detalhado anteriormente

* ScummVM: Habilitada emulação MT-32 para uma música melhor nos jogos da versão PC 

* DosBox: atualizado para r4076 + adicionado um teclado virtual onde você pode mapear teclas para os botões do joystick e jogar qualquer jogo do DOS em um controle! Veja [aqui](https://gitlab.com/recalbox/recalbox/issues/363) para maiores informações. Para os preguiçosos: tecle CTRL+F1

* Atualizado FBA-LIBRETRO para correções de bugs

* Resolvido um pequeno bug na nomenclatura padrão de roms do MAME

* Atualizado PPSSPP para v1.5.4

* Restaurado o vídeo de inicialização original + adicionados novos que esperamos que vocês gostem ;) Eles são selecionados aleatoriamente durante o boot

* Corrigido pequeno erro de grafia para aquivos .m3u multidisk do x68000

* Você pode iniciar alguns jogos do web manager

* Adicionada a palavra chave MUSIC nos compartilhamentos de rede em `/boot/recalbox-boot.conf`

* ES: subdiretórios de músicas serão lidos, corrigidos alguns bugs de inicialização, somente iniciar o Kodi na systems view, seletor de SSID Wifi, corrigida tela preta que ocorria ao sair de emuladores

Divirtam-se com este novo lançamento, e compartilhe seu feedback no forum !
