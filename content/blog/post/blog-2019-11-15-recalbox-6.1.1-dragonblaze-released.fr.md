+++
date = "2019-11-15T07:00:46Z"
title = "Recalbox 6.1.1 disponible"
image = "/images/blog/2019-11-15-recalbox-6.1.1-dragonblaze-released/Software_Box_Mockup_1_RECALBOX.jpg"

[author]
  name = "OyyoDams"
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  facebook = "https://www.facebook.com/oyyodams/"
  twitter = "https://twitter.com/OyyoDams"
+++

Recalbox s'améliore encore !

Depuis la sortie de la version 6.0, vous n’avez cessé de nous faire part de vos remerciements, mais également de retours et remarques afin d’améliorer chaque jour un peu plus le projet Recalbox.
Voici un résumé des derniers changements !


## Recalbox 6.1: DragonBlaze

La version 6.0 était une révolution, mais comme nous voulons toujours en faire plus, cette nouvelle version estampillée « 6.1 » apporte son petit lot de nouveautés et porte à près de 95 le nombre de systèmes désormais émulés et pris en charge par Recalbox, en y ajoutant une quinzaine de nouvelles machines ! 
Parmis elles, les Uzebox, Amstrad GX4000, Apple IIGS, Amiga 600 et 1200, Spectravideo, Sharp X1, Palm, PC-88, TIC-80, Thomson MO6, Olivetti Prodest PC128, MSXturboR, Multivision, mais aussi des très attendus systèmes arcade Atomiswave*, NAOMI*, et même la console Saturn* (*uniquement sur PC pour le moment) 

Recalbox 6.1 marque aussi l’arrivée très plébiscitée des snaps vidéo, de petits aperçus vidéos qui se lancent automatiquement lorsque vous restez sur un jeu dans votre liste. Cette fonctionnalité est idéale pour permettre aux utilisateurs de découvrir certains titres et d’avoir rapidement une idée du gameplay au premier coup d’oeil !

Recalbox 6.1 marque le retour tant attendu du scraper interne (programme permettant de récupérer toutes les méta-données d’un jeu comme sa jaquette, ses infos, son descriptif...), qui se dote au passage de nouvelles options !

En plus de la famille des Raspberry Pi Zero-W-H, 1, 2, 3B, 3B+ et Compute Module 3, des Odroid C2 et XU4 et des PC x86/x64, Recalbox 6.1 peut aussi désormais s’installer sur le dernier Raspberry Pi 3 A+, bonne nouvelle pour les makers !

Fruit d’un long travail de développement et d’optimisations drastiques, le Gpi Case est désormais nativement 100 % plug and play sur Recalbox 6.1. Nul besoin d’installer de scripts ou de faire le moindre paramétrage, tout est automatique et totalement optimisé pour coller aux performances modestes du Pi0/0W !
Pour cela, Recalbox 6.1 a subit une énorme cure d’amincissement et un entraînement quasi-militaire !

Le meilleur dans tout ça ? TOUTES LES CARTES profitent des améliorations et optimisations opérées pour le Gpi Case. On frôle désormais le démarrage instantané sur de nombreuses cartes, et le système a globalement gagné en fraîcheur, performance et réactivité… et ou..., qui peut le moins... peut le plus ! 

EN VRAC : 

    • RetroArch a été mis à jour en sa dernière version 1.7.8, sonnant la retraite de l'atroce menu vert RGUI, qui laisse place à la toute nouvelle interface sobre et moderne OZONE !     
    • … et introduit même les prémices de la traduction instantanée des jeux rétro, avec la fonction « text-to-speach »*, donnant pour la première fois de l’histoire LA PAROLE aux jeux rétro ! (*nécessite une configuration)
    • Configuration des pads Bluetooth plus facile que jamais
    • Support des fichier « .7z » pour de nombreux nouveaux émulateurs
    • Amélioration des différences de volume sonore entre les jeux et le menu Recalbox
    • FinalBurn Alpha devient FinalBurn Neo, suite à l’abandon du projet. (Romset toujours recommandé : 0.2.97.44, il ne change pas)
    • MAME évolue, et passe désormais en « mame2003_plus ». Bien que totalement rétrocompatible avec le romset Mame 0.78 utilisé jusqu’alors, on recommande désormais de passer sur le « romset 2003+ », ajoutant la compatibilité de près de 500 nouveaux jeux arcade ! 
    • Un mode DÉMO totalement amélioré, avec plein de nouveaux paramétrages comme le choix des systèmes, mais aussi l’ajout d’un écran de fin, pour rappeler au joueur le titre et le système du jeu lancé à l’instant, s’il souhaite y rejouer ! 


## Recalbox 6.1.1: DragonBlaze

Soyons clairs, n'attendez pas de grosses nouveautés avec Recalbox 6.1.1.
Avec tous les changements de la 6.1, et malgré nos (très) nombreux tests, vous nous avez remonté des bugs et instabilités. 
C’est pourquoi nous avons mis l’accent sur des correctifs afin de définitivement stabiliser Recalbox 6.1 avec ce bugfix estampillé "6.1.1", avant de vous réserver de nouvelles surprises avec Recalbox 6.2, mais ça, c'est une autre histoire... patience !

Nous avons ici entièrement revu le wifi et le bluetooth. 
L’ajout de manettes sans fil est plus facile et plus stable que jamais !

On a corrigé tout un tas de petits bugs sur XU4, PC, mais aussi l'utilisation de certains émulateurs comme les SNES, GX4000, Apple2, Atari...

Nous en avons également profité pour mettre à jour de nombreux core d'émulateurs afin de vous faire profiter des dernières fonctionnalités, et de nombreuses améliorations internes. 


## Téléchargement

Vous voulez essayer Recalbox 6.1.1 DragonBlaze ?

[Téléchargez-le!](https://archive.recalbox.com) C'est gratuit, open-source, soutenu par une équipe de support bienveillante et propulsé par une armée de bénévoles passionnés dans un but non-commercial (pour le fun !).

-----
