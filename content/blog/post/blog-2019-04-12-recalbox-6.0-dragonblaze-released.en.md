+++
date = "2019-04-12T07:00:46Z"
title = "Recalbox 6.0 released"
image = "/images/blog/2019-04-12-recalbox-6.0-dragonblaze-released/banner.jpg"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"

[translator]
  name = "Michael Baudino"
  twitter = "https://twitter.com/michaelbaudino"
  github = "https://github.com/michaelbaudino"
  gitlab = "https://gitlab.com/michaelbaudino"
+++

It's finally here 🙌

You have been waiting for it for months, we have been working on it for months.

We are today super proud to announce the general availability of Recalbox new version!

The latest Recalbox stable version is not `18.07.13` anymore: it is…

## Recalbox 6.0: DragonBlaze

As you already know if you read [our previous post about the Release Candidates]({{<ref "blog-2019-03-22-upcoming-stable-release.en.md">}}), we decided to change back our release numbering scheme (using dated version numbers was actually causing too much confusion 😖), the new stable version is thus numbered `6.0`.

We also decided that some releases would get assigned a nickname. And we are really happy to announce the birth of DragonBlaze 🐉 Discussing why, though, will not be part of this blog post… try an educated guess 😉

## Credits

This new stable version is the result of 9 months of hard labour from multiple people, from multiple countries and with multiple profiles. We are proud of this diversity and we are actively encouraging it: everyone is welcome to join & help 💪

The **development team** indeed provided substantial effort to onboard new emulators, implement new features and fix a tremendous amount of nasty bugs. A big thank you to all of them: historic members, new members and indeed former members.

But also to all **external contributors** who helped more-or-less temporarily but always with much caring and friendliness, coming in with their expertise, time, and willingness to help. We're really grateful for any contribution, small or big, technical or not, on code, design, documentation, … not to mention our team of volunteering translators 🌐

But beyond development, we tried to adopt more and more rapid pre-release cycles, gathered early-adopters feedback which drove us to this stable version. The key word here is "gather feedback": none of it would have been possible without the sustained attention and (mostly) endless patience of our **support team** on all our communication channels: [Discord](https://discord.gg/RR2jc5n) and [our forums](https://forum.recalbox.com), mostly, but also by [email](mailto:support@recalbox.com).

The third main piece of the team are **designers** 🎨 Recalbox wouldn't have the success we're seeing today without a slick, intuitive and modern interface. Big thanks to our tireless designers ❤️

Finally, our work would not be so valuable without our awesome **community**: people are giving feedback, helping each other, developing themes, contributing code, publishing articles and videos, or simply but delightly talking about us. It's an incredible ecosystem, and we are surprised everyday to witness how vibrant it's growing.

Thanks everyone, for making Recalbox what it is today 🙏

## What's new?

Alright, I see you want the real stuff, right?

So what's new? Why should you upgrade to this new version?

The actual changelog is too long and technical to be exhaustively reviewed here, but let's highlight a few challenges we particularly fancy to have overcome 🎁

### New boards supported

Probably the most requested feature of all! Recalbox is now officially working and supported on **Raspberry Pi 3B+**. This board is the new flagship of the Raspberry Pi Foundation and provides improved performances, still for the same affordable price.

But that's far from being the only new board we added support for!

In the Raspberry family, support for the lesser-known **Compute Module 3** has also been added.

Thanks to the awesome work from @mrfixit2001, Recalbox is now also available on a whole new family of premium boards (read: "high performance") from [Pine64](https://www.pine64.org): **Rock64** and **Rock64Pro** (and compatible cards **RockBox** and **Rock Pi4**).

### Demo mode

A much anticipated feature, specially from bartop owners, has been added and is called the **demo mode**!

When chosen as Recalbox screensaver, the demo mode will automatically start when you do not touch any controller button for a while (can be configured).

It **randomly picks a game in your collection and starts playing it** for a few minutes ✨

Sit back and relax, we're showing you games you didn't even know you had!

For an even smoother experience, we added an extra sweet to it just a few days before the release: by pressing on the `Start` button of any of your controllers, Recalbox will hand over the control to you, and you will be able to play this game right were it left off 🕹

### New emulators

What would Recalbox be without all the emulators it embeds?

We do not develop any emulator ourselves, but we have been hard at work integrating and adapting new emulators for Recalbox.

More than **30 new emulators** have been integrated, raising the number of emulated systems to more than 80 🚀 It includes:

* Atari 5200
* Atari 8Bit
* Intellivision
* SamCoupé
* Fairchild Channel F
* Super Famicon SatellaView
* Amiga CD32
* NeoGeoCD
* Jaguar
* and many more…

And you know what?

About the 50 emulators we already had? Well, we **updated all of them** to newer versions ✨

### Revamped controllers support

On a more technical touch, we rewrote most of the code that handles controllers, with a very particular care for bluetooth controllers (and even more particular for the 8BitDo ones).

We also added support for many new controllers, including the **GameCube MayFlash**, the **XinMo controller**, the **8BitDo M30** and more…

### Adaptive controller

Last but not least, it might not be the most technically impressive feature, but very likely the dearest to our heart, because we believe that video games and retro-gaming bring people together.

We have always steered Recalbox towards accessibility: financial accessibility (it's a free, open-source solution for cheap hardware), technical accessibility (it's a beginner-friendly, plug-and-play solution) and historical accessibility (it's a wayback machine to forgotten software legacy).

A few months ago, we added human accessibility to our mission and we wanted to make Recalbox available to everyone.

Everyone is indeed quite a wide scope, but there's something we knew we could do that would allow disabled people to play more than 40k games on more than 80 gaming systems from the last decades.

So we did it, and we are so proud of it.

In Recalbox 6.0 DragonBlaze, we added official, plug-and-play support for the recently released [**Xbox Adaptive Controller**](https://news.microsoft.com/stories/xbox-adaptive-controller) by Microsoft.

We strongly believe it's a huge leap towards disabled people integration and we really hope that, as we expect, it will bring people together.

## Download

Would you fancy giving Recalbox 6.0 DragonBlaze a try?

[Download it now!](https://archive.recalbox.com) It's free, open-source, comes with friendly support and is powered by an army of passionate volunteers in a non-comercial purpose (for fun!).

-----
