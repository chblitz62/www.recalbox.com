+++
date = "2017-02-21T16:20:16+02:00"
image = "/images/blog/2017-02-12-psp4.1/title.jpg"
title = "La PSP a été ajouté à Recalbox !"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++
<div><p dir="auto">Nous avons attendu trop longtemps pour ça mais voilà : la <strong>Playstation Portable</strong> est maintenant dans Recalbox!</p><p dir="auto">Nous avons intégré l'émulateur <a href="http://ppsspp.org" rel="nofollow noreferrer" target="_blank">ppsspp</a> dans <strong>recalbox 4.1.0</strong>. Vous pouvez maintenant jouer aux jeux PSP sur votre écran en HDMI! </p><p dir="auto">Certains jeux peuvent tourner mieux que d'autres sur Raspberry Pi 3. L'émulateur tourne à fond sur Odroid and X86.</p></div>
<div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-12-psp4.1/game-select-psp.jpg" target="_blank"><img src="/images/blog/2017-02-12-psp4.1/game-select-psp.jpg" alt="game-select-psp"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://gitlab.com/recalbox/www.recalbox.com/uploads/977c7bbc66b5aa4c88afc2f684c85ef4/castelvania-x-psp.jpg" target="_blank"><img src="https://gitlab.com/recalbox/www.recalbox.com/uploads/977c7bbc66b5aa4c88afc2f684c85ef4/castelvania-x-psp.jpg" alt="castelvania-x-psp"></a></div><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://gitlab.com/recalbox/www.recalbox.com/uploads/bc0e33fee3e46b4d8c1108c53615437c/menu-screen-psp.jpg" target="_blank"><img src="https://gitlab.com/recalbox/www.recalbox.com/uploads/bc0e33fee3e46b4d8c1108c53615437c/menu-screen-psp.jpg" alt="menu-screen-psp"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://gitlab.com/recalbox/www.recalbox.com/uploads/d7ff59a6d7a6c82e404f5c468eac7108/exit-screen-psp.jpg" target="_blank"><img src="https://gitlab.com/recalbox/www.recalbox.com/uploads/d7ff59a6d7a6c82e404f5c468eac7108/exit-screen-psp.jpg" alt="exit-screen-psp"></a></div><p dir="auto">Voici une petite vidéo de notre recalbox faisant tourner <strong>Castlevania : The Dracula X Chronicles</strong>:</p><br>  <div style="text-align: center"><iframe width="560" height="315" src="https://www.youtube.com/embed/LB2VYdE98rs" frameborder="0" allowfullscreen="">youtube video</iframe></div></div>