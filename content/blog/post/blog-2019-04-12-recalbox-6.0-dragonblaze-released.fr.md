+++
date = "2019-04-12T07:00:46Z"
title = "Recalbox 6.0 disponible"
image = "/images/blog/2019-04-12-recalbox-6.0-dragonblaze-released/banner.jpg"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"
+++

Elle est enfin là 🙌

Vous l'avez attendue pendant des mois, nous avons travaillé dessus pendant des mois.

Aujourd'hui, nous sommes super fiers d'annoncer la disponibilité de la nouvelle version de Recalbox !

La dernière version stable n'est donc plus la `18.07.13`: c'est la…

## Recalbox 6.0: DragonBlaze

Comme vous le savez peut-être déjà si vous avez lu [notre article précédent sur les Release Candidates]({{<ref "blog-2019-03-22-upcoming-stable-release.fr.md">}}), nous avons décidé de changer la numérotation des versions (les versions datées causaient trop de confusion 😖), la nouvelle version stable est donc numérotée `6.0`.

Nous avons aussi décidé que certaines versions auraient un petit nom. Nous sommes donc ravis de vous annoncer la naissance de DragonBlaze 🐉 On ne vous dit pas pourquoi, on vous laisse chercher 😉

## Remerciements

Cette nouvelle version stable est le résultat de 9 mois de dur labeur de différentes personnes, dans plusieurs pays et avec des profils divers. Nous sommes fiers de cette diversité et nous l'encourageons vivement : on invite tout le monde à nous rejoindre 💪

L'**équipe de développement** a bien sûr fourni un effort substentiel pour embarquer de nouveaux émulateurs, implémenter de nouvelles fonctionnalités et corriger un nombre incroyable de méchants bugs. Un grand merci à eux : les membres historiques, les nouveaux membres, et bien évidemment les anciens membres.

Merci également aux nombreux **contributeurs externes** qui aident à plus ou moins court terme mais toujours avec beaucoup d'attention et de bienveillance pour apporter leur expertise, leur temps et leur envie d'aider. Nous sommes incroyablement reconnaissants pour toutes ces contributions, petites ou grosses, techniques ou non, sur le code, la documentation… sans oublier notre équipe de traducteurs bénévoles 🌐

Mais au delà du développement, nous avons essayé d'adopter des cycles de pré-sorties de plus en plus courts, afin de recueillir les retours des testeurs le plus rapidement possible, ce qui nous permet de sortir aujourd'hui cette version stable. Le mot-clef ici est "recueillir les retours utilisateurs" : rien de tout celà ne serait possible sans la surveillance permanente et la patience (presque) infinie de notre **équipe de support** sur tous les canaux de communication : [Discord](https://discord.gg/RR2jc5n) et [nos forums](https://forum.recalbox.com) principalement, mais aussi par [email](mailto:support@recalbox.com).

La troisième pièce principale de notre équipe est composée des **designers** 🎨 Recalbox ne serait pas le succès qu'on lui connait aujourd'hui sans son interface léchée, intuitive et moderne. Un grand merci à tous nos inépuisables designers ❤️

Enfin, notre travail n'aurait pas la valeur qu'il a sans notre formidable **communauté** : des gens nous font des retours, d'autres s'entraident sur les forums, développent des thèmes, contribuent au code, publient des articles et des vidéos, ou tout simplement parlent de nous autour d'eux avec un plaisir communicatif. C'est un écosystème incroyable qu'on se surprend tous les jours à voir grandir et vibrer.

Merci à toutes et à tous, de faire de Recalbox ce que c'est aujourd'hui 🙏

## Quoi de neuf ?

OK, je vois que vous voulez les vraies infos, c'est ça ?

Alors, quoi de neuf ? Pourquoi devriez-vous mettre à jour votre Recalbox vers cette version ?

Le changelog complet est trop long et technique pour être détaillé de manière exhaustive ici, mais voyons quelques fonctionnalités qu'on est particulièrement fiers d'avoir ajoutées 🎁

### Nouvelles cartes compatibles

Probablement la fonctionnalité la plus demandée ! Recalbox est désormais officiellement compatible avec le **Raspberry Pi 3B+**. Ce nano-ordinateur est le nouveau vaisseau amiral de la Fondation Raspberry Pi et fourni de meilleures performances pour un prix toujours abordable.

Mais c'est loin d'être la seule nouvelle carte supportée !

Dans la famille Raspberry, nous avons ajouté le support pour le moins connu **Compute Model 3**.

Grâce à l'excellent travail de @mrfixit2001, Recalbox est maintenant également disponible sur toute un famille de cartes premium (lisez : "haute performance") de [Pine64](https://www.pine64.org) : **Rock64** et **Rock64Pro** (et les cartes compatibles **RockBox** et **Rock Pi4**).

### Mode démo

Une fonctionnalité très attendue, notamment par les propriétaires de bornes d'arcade, a été ajoutée et s'appelle le **mode démo** !

Quand on choisit le mode démo comme économiseur d'écran de Recalbox, après une période d'inactivité **un jeu choisi au hasard dans votre collection est lancé** pour quelques minutes ✨

Détendez-vous et profitez-en pour découvrir des jeux que vous ne saviez même pas que vous aviez !

Pour une expérience encore plus agréable, on a ajouté une douceur de dernière minute au mode démo : si vous appuyez sur la touche `Start` d'une des manettes, le mode démo de votre Recalbox va vous rendre la main et vous laisser jouer tranquillement au jeu qui est en train d'être présenté, comme si c'est vous qui l'aviez lancé 🕹

### Nouveaux émulateurs

Que serait Recalbox sans ses émulateurs ?

Nous ne développons pas les émulateurs nous-même, mais nous passons du temps à intégrer et adapter de nouveaux émulateurs pour Recalbox.

Plus de **30 nouveaux émulateurs** on été ajoutés, augmentant le nombre total de systèmes émulés à plus de 80 🚀 Notamment :

* Atari 5200
* Atari 8Bit
* Intellivision
* SamCoupé
* Fairchild Channel F
* Super Famicon SatellaView
* Amiga CD32
* NeoGeoCD
* Jaguar
* et bien d'autres…

Et vous savez quoi ?

Les 50 émulateurs qu'on avait déjà? Et bien, on les a **tous mis à jour** vers des versions récentes ✨

### Refonte du support des manettes

À un niveau plus technique, nous avons ré-écrit une bonne partie du code qui gère les manettes, avec un soin particulier pour les manettes bluetooth, et tout particulier les 8BitDo.

Nous avons également ajouté le support pour de nombreuses manettes, y compris les **MayFlash GameCub**, le contrôleur **XinMo**, la **8BitDo M30** et d'autres…

### Manette adaptive

Enfin, mais pas des moindres, ce n'est pas l'innovation la plus impressionnante techniquement, mais très probablement celle la plus chère à nos coeurs, parce qu'on croit fermement que les jeux vidéos et le retro-gaming rapprochent les gens.

Nous avons toujours guidé Recalbox dans le sens de l'accessibilité : accessibilité financière (c'est une solution libre et gratuite pour du matériel abordable), accessibilité technique (c'est une solution plug-and-play et facile à prendre en main) et accessibilité historique (c'est une machine à remonter le temps vers un héritage logiciel oublié).

Il y a quelques mois, nous avons décidé d'ajouter l'accessibilité humaine à notre mission et on a voulu rendre Recalbox accessible à tout le monde.

Et tout le monde, c'est évidemment assez grand, mais il y a quelque chose qu'on savait pouvoir faire et qui pourrait donner accès aux personnes atteintes de différents handicaps à 40.000 jeux sur plus de 80 consoles des dernières décennies.

On l'a donc fait, et on n'en est pas peu fiers.

Dans Recalbox 6.0 DragonBlaze, nous avons ajouté le support officiel pour la toute nouvelle [**manette Xbox Adaptive**](https://news.microsoft.com/stories/xbox-adaptive-controller) de Microsoft.

On croit fermement que c'est un pas de géant vers l'intégration des personnes atteintes de handicaps et on espère de tout coeur que - comme on s'y attend - ça fera se rapprocher les gens.

## Téléchargement

Vous voulez essayer Recalbox 6.0 DragonBlaze ?

[Téléchargez-le!](https://archive.recalbox.com) C'est gratuit, open-source, soutenu par une équipe de support bienveillante et propulsé par une armée de bénévoles passionnés dans un but non-commercial (pour le fun !).

-----
