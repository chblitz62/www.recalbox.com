+++
date = "2017-12-03T11:00:00+02:00"
image = "/images/blog/2017-12-03-recalbox-17-12-02/recalbox-17.12.02-banner.jpg"
title = "Recalbox 17.12.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  twitter = "https://twitter.com/digitalumberjak"
  name = "digitalLumberjack"

[translator]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++

Salut!

Nous avons sorti la version `17.12.01` vendredi dernier et des utilisateurs ont détecté des lags en jeu et dans Emulsationstation.

Cela était du au service "steam controller" qui était constamment exécuté au boot, même si vous ne possédiez pas ce dernier.

Nous avons corrigé ce comportement et vous pouvez dès à présent mettre à jour votre Recalbox en version `17.12.02`.

Pour effectuer une nouvelle installation, les images de Recalbox sont disponibles sur [archive.recalbox.com](https://archive.recalbox.com)
