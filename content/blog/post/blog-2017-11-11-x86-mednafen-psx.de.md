+++
date = "2017-11-11T14:00:00+00:00"
image = "/images/blog/2017-11-11-mednafen-psx/recalbox-mednafen-psx-banner.png"
title = "Neue PSX Cores für X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo ihr Lieben,

Der momentan einzige PSX Core für Recalbox ist großartig für ARM Hardware.
Aber er funktioniert nicht mit erweiterten Renderingeinstellungen für X86 builds.

Um dieses Problem zu beheben, haben wir die [libretro mednafen PSX Cores](https://github.com/libretro/beetle-psx-libretro) aufgenommen.

Der **mednafen_psx_hw** Core ist die um den OpenGL-Renderer erweiterte Version. Er benötigt eine GPU, die mit OpenGL 3.1 aufwärts kompatibel ist.
Sollte eure Hardware nicht kompatibel sein, könnt ihr den Standard-Core **mednafen_psx** verwenden.


Hier ein paar Optionen, die von diesen Cores unterstützt werden:

- OpenGL-Renderer
- Interne GPU Auflösung (1x(nativ)/2x/4x/8x/16x/32x)
- Textur-Filter (nearest/SABR/xBR/bilinear/3-point/JINC2)
- Interne Farbtiefe (dithered 16bpp (nativ)/32bpp)
- PGXP perspektivisch korrekte Texturierung
- Widescreen-Modus-Hack
- CPU Übertaktung
- Unterstützt CHD Spiele

Mehr Informationen in der [libretro Dokumentation](https://buildbot.libretro.com/docs/library/beetle_psx_hw/) (englisch).

Übrigens benötigen die Cores neue BIOS-Dateien. Hier die Dateien [für eure Recalbox](
https://github.com/recalbox/recalbox-os/wiki/F%C3%BCge-ein-System-BIOS-hinzu-%28DE%29):

- **8dd7d5296a650fac7319bce665a6a53c** - **scph5500.bin**
- **490f666e1afb15b7362b406ed1cea246** - **scph5501.bin**
- **32736f17079d0b2b7024407c39bd3050** - **scph5502.bin**

Die Cores sind wählerisch, also beachtet die Dateinamen.

Wir hoffen, ihr habt Spaß mit den neuen Cores und könnt die fantastische Spielevielfalt der PSX mit moderner Graphik (wieder)entdecken.


## Interne GPU Auflösung

[![Interne GPU Auflösung](/images/blog/2017-11-11-mednafen-psx/resolution-thumb.png)](/images/blog/2017-11-11-mednafen-psx/resolution.png)

## Textur-Filter

[![Textur-Filter](/images/blog/2017-11-11-mednafen-psx/textures-thumb.png)](/images/blog/2017-11-11-mednafen-psx/textures.png)

## PGXP perspektivisch korrekte Texturierung

[![PGXP perspektivisch korrekte Texturierung](/images/blog/2017-11-11-mednafen-psx/perspective-thumb.png)](/images/blog/2017-11-11-mednafen-psx/perspective.png)