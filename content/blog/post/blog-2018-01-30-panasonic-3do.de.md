+++
date = "2018-01-30T11:00:00+02:00"
image = "/images/blog/2018-01-30-panasonic-3do/recalbox-panasonic-3do-banner.png"
title = "Panasonic 3DO"

[author]
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  name = "OyyoDams"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo ihr Lieben!

Wir freuen uns, euch heute eine Konsole und deren Release zu präsentieren. Die Konsole ist trotz ihrer hervorragenden Performance eher unbekannt: Das **Panasonic 3DO**!


Leider wird es das System nur für die Odroid XU4 und PC X86/X86_64 Versionen von Recalbox geben. Selbst mit den niedrigsten Einstellungen und Frameskip war es uns nicht möglich eine akzeptable Emulation auf einem Raspberry Pi zu erzielen. Daher haben wir entschieden das System in den anderen Recalbox-Versionen nicht zu aktivieren.


Das System wird mittels [libretro 4do](https://github.com/libretro/4do-libretro) Core emuliert, welcher auf dem [4DO](http://www.freedo.org/) Emulator basiert.


Beachtet, dass für den neuen Core auch ein neues BIOS hinzugefügt werden muss. Hier findet ihr die passenden [Dateien für eure Recalbox](https://github.com/recalbox/recalbox-os/wiki/F%C3%BCge-ein-System-BIOS-hinzu-%28DE%29):
 
- **51f2f43ae2f3508a14d9f56597e2d3ce** - **panafz10.bin**


Wir hoffen, ihr habt Spaß an dem neuen Core und könnt damit die 3DO Spielesammlung (wieder)entdecken.


## Ein Video-Review der 30 besten Panasonic 3DO Spiele

<iframe width="560" height="315" src="https://www.youtube.com/embed/un6u1nmvyTo" frameborder="0" allowfullscreen></iframe>
