+++
date = "2018-06-27T01:00:00+02:00"
image = "/images/blog/2018-06-27-recalbox-18-06-27/recalbox-18.06.27-banner.png"
title = "Recalbox 18.06.27"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Hey everyone!

It's been some time since our last release ... Guess why it took so long for a new version? Because we've been working really hard on a very unique feature that has been integrated "the Recalbox way"! Yeah boys! I guess you all know what **netplay** means, uh? It's now integrated in ES in an easy-to-use GUI!

Now let's go in depths of the changelog:

* **Adding introRecalboxEASports.mp4**: With the FIFA world up, we thought it would be funny to add a Recalbox version of a famous EA intro ;)

* **ES: Added Netplay option in menus + menus to add roms hashs in gamelists**: this concerns basic netplay configuration

* **ES: Added Netplay : GUI for client mode and button to launch as host**: this will all be detailled in the wiki + netplay specific blogpost. In short: a nice window to list existing netplay games and easily join them, with some icons showing if you can join or not. And of course, starting a game as server is fairly easy too! Remember one thing the X (SNES layout) button is your one way button for everything regarding netplay! You must remember the limits though: no netplay for handhelds or 3D consoles + some libretro cores may be too demanding (CPU/RAM) to work on Pi (for ex: Snes9x won't launch on pi3 in netplay mode due to a lack of RAM)

* **ES: Fixed clock not in local time**: yeah, was set on GMT, no matter where you live

* **ES: Fixed old themes crash**: our bad, sorry, ES was crashing when you were trying to change themes using the old specs

* **ES: Option for popup position**: you can decide on which corner of the screen you display it!

* **ES: Help messages in popups**: help messages used to be a good old dialog box, they are now a pop-up!

* **ES: Slider for popup duration**: you can now decide how long a popup will be displayed

* **ES: Languages names in their own language**: self explanatory ;)

* **ES: Refactored gamepad configuration panel**: pad configuration has been errr ... a little modernized, see the dedicated blogpost

* **ES: Update Emulator / Core selection process to avoid gui inception and to display default values**: should be a little easier to read and understand + show the default values

* **ES: Update font size on game metadata gui**: self explanatory

* **ES: Remove dead code of RomsManager**: removing some unsued code means a smaller memory print

* **ES: Make gamelist sort persistent**: The game list sort was lost on reboot, when changing others options... it is now save and persistant

* **ES: Show folder content in detail panel**: When selecting a folder in the game list, the previous game data was not refreshed, it now show an exrtact of the games contained in the folder (fully themable)

* **ES: Show single folder game as game in parent list**: Folders containing a single game (i.e. PSX, Dreamcast...) are now shown as game, you no longer need to select the folder to see and run the game in it

* **ES: Added an option to list folder content instead of folder name**: This options allows to show games contained in complex folders structure in a single game list, hidding all folder

* **ES: Only display available letter in JumpToLetter + Append figures**: The "Jump to letter" option now only show available letters and figures

* **ES: Avoid reload of images if source doesn't changed**: Simple display optimisation

* **ES: Update Games lists from GameList menu**: The "Update Game list" feature is now available directly in the system

* **ES: Set Menu list in loop mode**: Go directly to the bottom of a list by using the top arrow on the first entry, classic, but, what a pitty to no have it till now!

* **ES: Fixed back button in gamelist**: Using back in a deep folder used to go back to the root level, it now go to the level - 1

* **ES: Fixed back from game launch**: Leaving a game used to go the root level of the game list, it now stay where it was

* **ES: Fixed detailed panel missing when jumping to letter A**: Jumping to the first game didn't refresh the detailed game panel, it's now fix

* **ES: Fixed set a game favorite for the first time (required two switches before)**: Depending on the game list, adding a game to favorite sometime used to need to toggle it twice

* **ES: Stay is selected sub-folder when changing sort type / order in gamelist**: Sorting a game list used to go back to the root level of the game list, it's now fixed

* **Bump kodi-plugin-video-youtube to 5.5.1**: this plugin evolves way too fast but also doesn't keep a very long history of versions, so we must bump it

* **recalbox-config.sh: added getEmulatorDefaults**: just a helper for devs to know what is a system default emulator

* **Added fonts to support all languages**: it should now work all fine! Any language from the whole world should display fine now.

* **picodrive: partially rewrite package + re-enable for odroidc2**: C2 users will be happy, picodrive is back for them!

* **Add alternative methods to detect sound cards**: this has no impact yet, but will help for new boards which don't declare soundcards as others

* **Boot time optimization**: rewrote some sluggish timeout, ES will start loading faster!

* **Wifi: option to change region added to recalbox.conf - with wifi.region=FR fixes channel 12 issue for example**: french users know what this means: we are finally not restricted anymore to US wifi channels, meaning we can finally use channels 12 and 13 with the Pi3 internal WiFi!

* **Old deprecated API removed, new API in development**: the old API is not maintained for 2 years, can't even start since Recalbox 4.1, so just drop it

* **more informations in support archives**: that's to help us to give better support

* **sound: try to fix configuration loss on upgrade**: this is a nasty bug on PC where upgrading Recalbox would drop the audio configuration. This should happen once for the last time with this upgrade, but shouldn't happen anymore after.

* **Add message to warn before turning off Recalbox**: some people were turning their recalbox way too early, thus resulting on some data corruption. This should tell them at least not turn it their Recalbox off too early

* **Add Daphne system using Hypseus emulator**: enjoy laserdisc games!

* **pin356ONOFFRESET: improved behaviour, more compatible with nespi+ case**: just set the powerswtich type to pin356ONOFFRESET to enjoy your nespi+ case

* **Add splash screen duration option in recalbox.conf**: you can now tell Recalbox how long it should play splash videos : as usual, for a defined time, or until the end of the video (which holds the loading of ES then)

* **Bump mGBA emulator and add it to gb and gbc for Super Game Boy support**: mGBA can play GameBoy and GameBoy Color roms in super gameboy format. The super gameboy was a SNES device to play GB roms on your SNES !

* **Add Thomson TO8(D) support with libretro core theodore, thank you zlika**: The very first computer I ever started to use in my life and showed me that I'd work in IT once grown-up!

* **linapple: support apple2.configfile=dummy to avoid overwriting configuration**: you can tell Recalbox not to generate the linapple configuration files now

* **Add Amstrad CPC core: crocods**: this one has no virtual keyboard though, but works pretty fine

* **New version of libretro-o2em now supports save states and rewind**: self explanatory!

* **Bump all libretro cores**: that's better for netplay :D

* **Bump Retroarch to 1.7.3 + patch required parts**: mandatory! I strongly recommend you read [the libretro blog](https://www.libretro.com/index.php/retroarch-1-7-3-released/) to get all the details. Some people should have noticed that you can use 15kHz monitors on PC now with the right settings ;) We havn't had time to experiment with it sadly, but we'll definitely give it a go as soon as possible! Please share your feedback on the forum!

* **configgen: N64 back to fullscreen the real way, without forcing resolution**: until now, we had to simulate a window of the size of the screen to prevent some audio bug. Now we're back to some real fullscreen mode with mupen64plus

* **configgen: the video syntax "auto DMT 4 HDMI" now works for N64**: the new `auto` specific option where you can specify a resolution to be set if your monitor supoprts it (pi only feature) also works for N64 now

That's a biiiiig changelog, and we're very proud of our work with Netplay! We hope you'll enjoy this new release and share your experience! I strongly recommend you also activate retroachivements with netplay, both are compatible!
