+++
date = "2019-04-12T07:00:46Z"
title = "Recalbox 6.0"
image = "/images/blog/2019-04-12-recalbox-6.0-dragonblaze-released/banner.jpg"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"

[translator]
  name = "Robsdedude"
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
+++

Endlich ist sie da 🙌.

Ihr habt Monate lang gewartet, wir haben Monate lang daran gearbeitet.

Stolz wie Bolle verkünden wir hiermit, dass eine neue Version von Recalbox verfügbar ist!

Die neuste stabile Version ist ab jetzt nicht mehr `18.07.13` sondern…

## Recalbox 6.0: DragonBlaze

Wenn ihr [unseren vorigen Blog-Post](/blog/post/blog-2019-03-22-upcoming-stable-release/) zum Release-Candidate gelesen habt, wisst ihr bereits, dass wir die Versionen wieder durchnummerieren (das Datum als Versionsname hat zu viel Verwirrung gestiftet 😖). Daher lautet die neue Versionsnummer `6.0`.

Weiter haben wir uns entschieden manchen Versionen Nicknames zu geben. So sind wir stolz dir Geburt von DragnBlaze 🐉 zu verkünden. Allerdings werden wir hier nicht erläutern, wie es zu dem Namen kam… Drei mal dürft ihr raten 😉.

## Credits

Die neue Version ist das Ergebnis neun Monate harter Arbeit vieler Leute aus vielen Ländern mit verschiedensten Profilen. Wir sind stolz auf diese Diversität und möchten sie fördern: Jeder ist eingeladen mitzumachen und mitzuhelfen 💪.

Das **Entwickler-Team** hat sich ins Zeug gelegt, um neue Emulatoren an Bord zu bekommen, neue Features zu implementieren und eine enorme Menge an Bugs zu beseitigen. Einen riesen Dank an sie alle: Mitglieder seit Langem, neue Mitglieder und auch ausgeschiedene Mitglieder.

Ebenso an alle **externen Mitwirkenden** die mal kürzer- mal längerfristig aber immer mit viel Einsatz, Freundlichkeit, Expertise, Zeit und Wohlwollen geholfen haben. Wir sind für jeden Beitrag sehr dankbar, egal ob groß, klein, technischer Natur oder nicht, am Code, am Design, an der Dokumentation, … Nicht zu vergessen das Team unserer ehrenamtlicher Übersetzer 🌐.

Neben dem Entwickeln haben wir zunehmend schnelle Pre-Release-Cycles und Feedback von Beta-Testern eingarbeitet, was uns schließlich zu diesem stable Release führte. Das Schlüsselwort hier ist "Feedback einholen": Nichts von alle dem hier wäre möglich ohne die andauernde Aufmerksamkeit und endlose Geduld unseres **Support-Team**s, die über all unsere Kommunikationskanäle erreichbar sind: hauptsächlich [Discord](https://discord.gg/RR2jc5n) und [unser Forum](https://forum.recalbox.com) aber auch per [Mail](mailto:support@recalbox.com).

Der dritte große Teil unseres Teams sind die **Designer** 🎨. Recalbox wäre ohne die schlanke, intuitive und moderne Oberfläche bei weitem nicht so Beliebt. Ein großer Danke an unsere unermüdlichen Designer ❤️.

Letztendlich wäre diese Arbeit aber nichts wert, wäre da nicht unsere super **Community**: Ihr gebt uns Feedback, helft euch untereinander, entwickelt Themes, tragt zum Code bei, veröffentlicht Artikel und Videos oder helft uns, indem ihr euren Freunden von uns erzählt. Ein unglaubliches Ecosystem – uns erstaunt, wie lebhaft es tagtäglich weiterwächst.

Danke an alle. Wegen euch ist Recalbox, was es heute ist 🙏.

## Was gibt's Neues?

Na gut, genug Gelaber? Kommen wir zum eigentlichen Inhalt.

Was ist neu? Warum solltet ihr auf die neue Version aktualisieren?

Der Changelog ist zu lang und technisch, um ihn hier ausgiebig zu besprechen. Stattdessen werden wir hier ein paar Probleme hervorheben, deren Bewältigung uns besonders erfreut 🎁.

### Neu unterstützte Boards 

Das am häufigsten nachgefragte Feature von allen: Recalbox funktioniert jetzt endlich auf dem **Raspberry Pi 3B+**. Es handelt sich dabei um das neue Flaggschiff der Raspberry Pi Foundation und bietet mehr Leistung für einen gleichermaßen günstigen Preis.

Das ist aber bei Weitem nicht das einzige neue Board, das wir jetzt unterstützen!

Auch das weniger bekannte **Compute Module 3** der Raspberry-Familie wird jetzt unterstützt.

Dank der wunderbaren Arbeit von @mrfixit2001 läuft Recalbox jetzt auch auf einer ganz neuen Familie an Premium-Boards (sprich: "high performance") von [Pine64](https://www.pine64.org): **Rock64** und **Rock64Pro** (und die kompatiblen **RockBox** und **Rock Pi4**).

### Demo-Modus

Ein lang herbeigesehntes Feature, besonders von Bartop-Besitzern, nämlich ein **Demo-Modus**, wurde hinzugefügt!

Wenn der Demo-Modus als Bildschirmschoner eingestellt wird, startet er automatisch nach einer einstellbaren Zeit, in der kein Button am Controller gedrückt wird.

Dann wird für ein paar Minuten ein **zufälliges Spiel eurer Sammlung gestartet und autonom gespielt** ✨.

Lehnt euch zurück und entspannt euch, während wir euch Spiele zeigen, von denen ihr noch nicht einmal wusstet, dass ihr sie habt!

Für ein noch besseres Erlebnis haben wir gerade ein paar Tage vor dem Release noch ein Extra implementiert: Wenn ihr den `Start`-Button auf einem eurer Controller drückt, wird euch Recalbox die Kontrolle überreichen und ihr könnt das Spiel ab diesem Moment selbst weiterspielen 🕹.


### Neue Emulatoren

Was wäre schon Recalbox ohne all die eingebetteten Emulatoren?

Wir entwickeln diese nicht selbst, aber wir haben energisch daran gearbeitet neue Emulatoren anzupassen und in Recalbox zu integrieren.

Über **30 neue Emulatoren** wurden integriert, was die Anzahl der emulierten Systeme auf über 80 anhebt. Darunter:

* Atari 5200
* Atari 8Bit
* Intellivision
* SamCoupé
* Fairchild Channel F
* Super Famicon SatellaView
* Amiga CD32
* NeoGeoCD
* Jaguar
* und viele mehr…

Außerdem haben wir **alle 50 bestehenden Emulatoren aktualisiert** ✨.


### Bessere Controller-Unterstützung

Etwas technischer: Wir haben den Großteil des Codes für die Controller neugeschrieben und dabei ein besonderes Augenmerk auf Bluetooth-Controller gelegt (insbesondere die 8BitDo-Controller).

Außerdem unterstützt Recalbox viele neue Controller. Darunter der **GameCube MayFlash**, der **XinMo Controller**, der **8BitDo M30** und mehr…


### Adaptive Controller

Zu guter Letzt, auch wenn es technisch gesehen nicht das beeindruckendste Feature ist, lag es uns doch sehr am Herzen, da wir glauben, dass Retro-Spiele Leute zusammen bringen.

Wir haben mit Recalbox schon immer auf Accessibility – also Barriere-Freiheit – abgezielt: Finanzielle Accessibility (kostenlose Open-Source-Lösung für günstige Hardware), technische Accessibility (Anfängerfreundliche Plug&Play-Lösung) und Historische Accessibility (eine Zeitmaschine für vergessenes Software-Kulturgut).

Ein paar Monate zuvor haben wir unserer Mission menschliche Accessibility hinzugefügt. Wir wollen Recalbox für alle zugänglich machen.

Nun ist *jeder* wirklich eine ganze Menge. Aber da gab es eine Möglichkeit Menschen mit Behinderung in den Genuss von mehr als 40k Spielen auf mehr als 80 Spiele-Systemen der letzten Dekaden zu bringen. Also sind wir es angegangen und wir sind stolz darauf.

Recalbox 6.0 DragonBlaze bietet offiziellen Plug&Play-Support für den kürzlich veröffentlichten [**Xbox Adaptive Controller**](https://news.microsoft.com/stories/xbox-adaptive-controller) (en) von Microsoft. [Hier](https://www.microsoft.com/de-de/p/xbox-adaptive-controller/8nsdbhz1n3d8) gibt es grundlegende Infos auf deutsch.

Wir sind überzeugt, dass das ein wichtiger Schritt in Richtung Integration von Menschen mit Behinderung ist und wir hoffen, dass es wie erwarten Menschen zusammen bringt.


## Download
Also, möchtet ihr Recalbox 6.0 DragonBlaze ausprobieren?

[Jetzt herunterladen](https://archive.recalbox.com)! Es ist kostenlos, open-source, hat einen freundlichen Support und wird von einer Armee aus leidenschaftlichen Ehrenamtlichen nicht-kommerziell (aus Freude) vorangetrieben.

-----
