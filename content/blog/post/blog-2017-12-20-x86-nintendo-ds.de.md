+++
date = "2017-12-20T10:00:00+00:00"
image = "/images/blog/2017-12-20-nintendo-ds/recalbox-nintendo-ds-banner.png"
title = "Nintendo DS für X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo Leute,

es ist an der Zeit, ein neues System auf Recalbox **X86/X86_64** zu begrüßen: **den Nintendo DS!**

Um das zu bewerkstelligen, haben wir zwei neue libretro Cores hinzugefügt. [libretro desmume](https://github.com/libretro/desmume) und [libretro melonDS](https://github.com/libretro/melonDS), der auf einem neuen Nintendo-DS-Emulator basiert.

Um das neue System zu emulieren, **müsst** ihr über leistungsstarke Hardware verfügen. Wir haben den desmume Core so konfiguriert, dass er mit möglichst vielen Setups kompatibel ist. Sollte eure Emulation dennoch nicht in voller Geschwindigkeit laufen, könnt ihr versuchen, die Einstellungen im retroarch-Menü herabzusetzen (interne Auflösung, Frameskip, etc...). Auf der anderen Seite könnt ihr die Einstellungen natürlich auch erhöhen, um eine bessere graphische Darstellung zu erhalten, sollte eure Hardware dies erlauben.

Leider ist der DS nur für X86 verfügbar, da selbst das stärkste ARM-Board, das von Recalbox unterstützt wird (der Odroid XU4), unter niedrigsten Einstellungen und mit Frameskip keine Emulation in voller Geschwindigkeit bietet.   
Daher haben wir uns entschlossen, das System nicht für andere Recalbox-Versionen zu aktivieren.

Zur Information: **libretro melonDS core** benötigt ein zusätzliches BIOS. Hier findet ihr die Dateien, die [eure Recalbox benötigt](https://github.com/recalbox/recalbox-os/wiki/F%C3%BCge-ein-System-BIOS-hinzu-%28DE%29):

**df692a80a5b1bc90728bc3dfc76cd948** - **bios7.bin**   
**a392174eb3e572fed6447e956bde4b25** - **bios9.bin**   
**e45033d9b0fa6b0de071292bba7c9d13** - **firmware.bin**   

Wir hoffen, ihr habt Spaß mit den neuen Cores und könnt eure Nintendo DS Spiele-Bibliothek (wieder)entdecken.


## Verschiedene Anzeige-Modi (standard/hybrid)

Standard:   
[![display_standard](/images/blog/2017-12-20-nintendo-ds/display-standard-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-standard.png)

Hybrid:   
[![display_hybrid](/images/blog/2017-12-20-nintendo-ds/display-hybrid-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-hybrid.png)

## Retro Shader

[![shader_retro_mario](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario.png)
[![shader_retro_kart](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart.png)

## Scanlines Shader

[![shader_scanlines_mario](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario.png)
[![shader_scanlines_kart](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart.png)
