+++
date = "2018-04-20T01:00:00+02:00"
image = "/images/blog/2018-04-20-recalbox-18-04-20/recalbox-18.04.20-banner.png"
title = "Recalbox 18.04.20"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Bonjour à tous !

On n'a pas relâché la pression sur Recalbox ces derniers temps pour vous proposer encore des nouveautés, dont beaucoup de changements sur EmulationStation. Vous pourrez constater une meilleure internationalisation de Recalbox grâce aux traductions de beaucoup de volontaires. Si vous souhaitez mettre la main à la pâte, [c'est ici pour EmulationStation](https://poeditor.com/join/project/hEp5Khj4Ck) ou [ici pour le webmanager](https://poeditor.com/join/project/taFNFlZ840).

On a aussi passé pas mal de temps sur une meilleure configuration du son. Dorénavant le changement du volume est dynamique mais il y a mieux : changement dynamique de la carte son ! Et ouais mes loulous, vous pouvez maintenant changer à la volée de carte son ! Le bug des niveaux de volumes n'est toujours pas résolu avec cette version, mais j'ai bon espoir pour la prochaine.

Donc on attaque avec le changelog en détail :

* **ES: ajout de pop-ups** Pour le moment on se contente d'afficher simplement le nom du morceau en cours, mais les bases sont posées. C'est quelque chose qu'on va améliorer et surtout mettre à profit dans le futur.

* **ES: Ajout d'un tag de transition dans les themes** Certains thèmes (comme Fuzion) ont l'air plus sympa avec une transition particulière. Il y a donc un tag pour prendre en compte cette nécessité.

* **Mise à jour de kodi-plugin-video-youtube to 5.4.6** C'est juste histoire de le mettre à jour, la version d'avant n'est tout simplement plus disponible et nous plante la compilation.

* **Mise à jour de Advancemame en 3.7 avec support des spinners** Une petite MàJ d'AdvanceMAME qui gère maintenant les souris, ce qui devrait arranger les spinners et sûrement les lightguns.

* **ES: Fixed audio card change** Celle-là va vous plaire ! Vous pouvez dynamiquement changer de carte son dans ES. Les utilisateurs PC vont s'en réjouir ;)

* **fix: police par défaut de RA et ES déplacée** LE bug des carrés jaunes, vous vous souvenez ? C'est pour éviter son retour car certains chemins ont changé.

* **ES: Ajout du changement dynamique de volume** Il parle delui-même lui, non ? Inutile de quitter la fenêtre pour que la valeur soit prise en compte, ca s'adapte direct avec le curseur !

* **ES: Ajout de ubuntu_condensed.ttf comme fonte de repli** Pour une meilleure prise en compte des alphabets autres que latin. On est toujours ennuyé avec les caractères Coréens celà-dit ...

* **ES: Ajout d'une horloge sur le menu principale** Appuyez sur Start pour voir l'heure ! S'active dans les options de l'interface.

* **ES: Ajout des icônes manquantes pour Amigas 3DO et X68k** Difficile de dire mieux ...

* **ES: Amélioration du rechargement des thêmes quand gamelistonly=0** Le thème devrait se charger un peu plus vite.

* **ES: Traduction des messages d'aide** Yeah, enfin !

* **ES: Ajout du message "QUIT" sur l'écran des systèmes** Le bouton SELECT n'avait aucune indication de son rôle. C'est résolu à présent.

* **retour de la version de PPSSPP à la précédente** Suite à une grosse demande pour revenir en arrière car la 1.5.4 posait de gros soucis à la majorité, on a suivi vos doléances.

* **Plus d'informations dans les archives de support** Pour nous ça, histoire de mieux vous dépanner.

* **Mise à jour : correction lors du passage d'une branche de test à la stable** On avait des problèmes pour passer d'une branche de tests à la stable. MAis c'est du passé maintenant !

* **recallog peut logger même quand le share n'est pas monté** Tellement mieux de pouvoir suivre en détail le boot. Ca va s'enrichir avec les versions à suivre.

* **fix: wifi qui était toujours démarré au boot même is désactivé** Ouais, on a raté un truc à un moment quelque part ...

* **séparation du montage du share et de la mise à jour** Tout était dans un seul script trop gros. Chacun dans son pré, c'est plus simple à maintenir

* **mame2010: ajout des hiscores** Woot! Mais le bug des ratio est toujours présent.

* **fix: fmsx ne pouvait pas être sélectionné pour le MSX** Maintenant fmsx est dispo !

* **DosBox: résolution d'un problème de lag sur pi3 + comportement bizarre si le dosbox.bat était absent** Ca remarche comme il faut maintenant !

* **SDL2: correction d'un bug qui bloquait ScummVM sur x86 au chargement** Vous allez pouvoir vous en donner à coeur joie !

* **Odroid XU4: stabilisation sur HDMI, meilleur boot.ini** Ceux utilisant un XU4 sur TV HDMI avec CEC pouvaient être ennuyés.

* **fix: le nom du core Game&Watch s'affiche correctement** Rien à redire

* **manager: mise à jour des traductions** Faut s'ouvrir au monde !

Sympa hein ? 
