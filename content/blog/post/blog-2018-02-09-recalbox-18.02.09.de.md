+++
date = "2018-02-09T11:00:00+02:00"
image = "/images/blog/2018-02-09-recalbox-18-02-09/recalbox-18.02.09-banner.jpg"
title = "Recalbox 18.02.09"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo Leute!

Das letzte Release liegt schon eine ganze Weile zurück. Es war Zeit, die freien Tage zu genießen, sich zu erholen und auch an neuen Features zu arbeiten.

Was kann man also von 18.02.09 erwarten? Hier die Liste der wichtigsten Neuerungen:

* Neue Controller werden nativ unterstützt: Ouya, Micreal Arcade Dual und Ipega 9055
* Ein neues Splash Video (danke @ian57)
* x86 (32 und 64 Bit) emuliert jetzt den DS mit den desmume und melonds libretro Cores.
* Der Mame2010 Core ist für alle Boards außer RPI 0 und 1 verfügbar.
* NAS Shares können via WLAN zur Bootzeit eingebunden werden. Konfiguriert WLAN und Share einfach, wie im [Wiki](https://github.com/recalbox/recalbox-os/wiki/Load-your-roms-from-a-network-shared-folder-%28EN%29#recalbox-version--41) (englisch) erklärt.
* Power Scripts aktualisiert (wenn GPIO-Buttons für die Power/Reset-Kontrolle verwendet werden).
* DOSBox und ScummVM aktualisiert
* Neuer Emulator: ResidualVM ([Homepage](http://www.residualvm.org) englisch). ResidualVM kann nur Grim Fandango, Escape from Monkey Island und Myst III abspielen. Bitte lest die Dokumentation auf der Website sorgfältig. Um diese Spiele zu spielen, kopiert sie mit der Datei-Endung `.residualvm` in den **scummvm** ROM-Ordner, wie ihr es mit einem ScummVM-Spiel tun würdet.
* Sharp x68000 via px68k libretro Core hinzugefügt. Sicherlich kannten damals nur wenige von euch dieses System ;)
* 3DO für XU4 und x86 32/64 hinzugefügt.

Wir hoffen, ihr habt Spaß mit der neuen Version!

Und... Oh mein Gott... Zeit für Steve Jobs berühmtes "One last thing" ["Eins noch"]...

Pi2 und Pi3 haben das Glück Amiga Support zu erhalten! Yeah Baby! Danke an unser Community-Mitglied Voljega für seine großartige, langjährige Arbeit an Amiga für Recalbox. Nun wird sie endlich ein Teil von Recalbox!