+++
date = "2017-11-11T14:00:00+00:00"
image = "/images/blog/2017-11-11-mednafen-psx/recalbox-mednafen-psx-banner.png"
title = "Novos cores PSX para X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá amigos,

O único core disponível para PSX no Recalbox tem feito um trabalho fantástico para placas ARM.
Contudo, ele não consegue efetuar melhorias de renderização para as versões X86.

Para corrigir esta limitação, adicionamos os [cores psx mednafen libretro](https://github.com/libretro/beetle-psx-libretro).

O core **mednafen_psx_hw** é aprimorado e efetua renderização e melhoramentos em OpenGL.
Ele requer no mínimo uma GPU compatível com OpenGL 3.1 para o correto funcionamento.
Caso seu hardware não seja compatível, voce pode usar o core padrão **mednafen_psx**.

Estas são algumas das opções disponíveis para estes cores(funcionalidades em inglês):

- OpenGL renderer
- Internal GPU Resolution (1x(native)/2x/4x/8x/16x/32x)
- Texture filtering (nearest/SABR/xBR/bilinear/3-point/JINC2)
- Internal color depth (dithered 16bpp (native)/32bpp)
- PGXP perspective correct texturing
- Widescreen mode hack
- CPU Overclock
- CHD games support

Maiores informações na [documentação da libretro](https://buildbot.libretro.com/docs/library/beetle_psx_hw/).

Para que vocês saibam, estes cores precisam de novos arquivos de bios. Estes são os arquivos a serem [adicionados ao seu Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) :

- **8dd7d5296a650fac7319bce665a6a53c** - **scph5500.bin**
- **490f666e1afb15b7362b406ed1cea246** - **scph5501.bin**
- **32736f17079d0b2b7024407c39bd3050** - **scph5502.bin**

Estes cores são exigentes, portanto, respeite os nomes de arquivos.

Esperamos que vocês curtam estes novos cores e (re)descubram a fantástica biblioteca de jogos de PSX com gráficos modernos.

## Internal GPU Resolution

[![Internal GPU Resolution](/images/blog/2017-11-11-mednafen-psx/resolution-thumb.png)](/images/blog/2017-11-11-mednafen-psx/resolution.png)

## Texture filtering

[![Textures filtering](/images/blog/2017-11-11-mednafen-psx/textures-thumb.png)](/images/blog/2017-11-11-mednafen-psx/textures.png)

## PGXP perspective correct texturing

[![PGXP perspective correct texturing](/images/blog/2017-11-11-mednafen-psx/perspective-thumb.png)](/images/blog/2017-11-11-mednafen-psx/perspective.png)
