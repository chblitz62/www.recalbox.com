+++
date = "2017-02-23T16:27:35+02:00"
image = "/images/blog/2017-02-23-help/title-help.jpg"
title = "New frontend Help Popups!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++
<p dir="auto">Hi, today let's intro introduce you a new feature from 4.1 release.</p><p dir="auto">When you are discovering and configuring your favorite gaming OS, it is not always possible to check our online manual to informations about each option.</p><p dir="auto">So, we have decided to add the possibility to display on screen, <strong>help pop-up</strong>, available on <strong>Y</strong> button of your gamepad.</p><p dir="auto">The pop-up are looking like this : </p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-23-help/es_help.jpg" target="_blank"><img src="/images/blog/2017-02-23-help/es_help.jpg" alt="es_help"></a></div><p dir="auto">Here is a short demo video:</p> <iframe width="560" height="315" src="https://www.youtube.com/embed/Tv3c_zC2Mfg" frameborder="0" allowfullscreen=""></iframe>
                  