+++
date = "2018-02-09T11:00:00+00:00"
image = "/images/blog/2018-02-09-amiga/recalbox-amiga-banner.png"
title = "Amiga"

[author]
  github = "https://github.com/voljega"
  gitlab = "https://gitlab.com/voljega"
  name = "Voljega"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá pessoal !

Após uma longa campanha de testes, a emulação Amiga foi finalmente integrada ao Recalbox !

Aqui vão detalhes sobre a funcionalidade:

* O emulador utilizado é o [Amiberry versão 2.1](https://github.com/midwan/amiberry).
* 2 sistemas são suportados agora: Amiga 500/600 (ambos no sistema a600) e o Amiga 1200. Amiga CD32 precisa de algumas melhorias no lado do Armberry (versão futura 2.5) e será integrado mais tarde.
* Os formatos suportados são CDs (.adf, carga automática de múltiplos discos) e disco rígido (WHDL).

Vocês precisarão de pelo menos 1 arquivo de BIOS dependendo do sistema que deseja emular:

* Amiga 600 ADF  **82a21c1890cae844b3df741f2762d48d  kick13.rom**
* Amiga 600 WHDL **dc10d7bdd1b6f450773dfb558477c230  kick20.rom**
* Amiga 1200 (ambos ADF e WHDL) **646773759326fbac3b2311fd8c8793ee  kick31.rom**

Para maiores informações, consulte o arquivo readme.txt localizado no diretório de roms específico e a [página da wiki](https://github.com/recalbox/recalbox-os/wiki/Amiga-on-Recalbox-%28EN%29) dedicada ao assunto.

Milhares de agradecimentos a Ironic e Wulfman pelo tempo investido em ajuda e testes !

Aqui vai um vídeo para relembrar vocês de alguns dos melhores jogos :

<iframe width="560" height="315" src="https://www.youtube.com/embed/SrO9iJp8e2g" frameborder="0" allowfullscreen></iframe>
