+++
date = "2017-12-20T10:00:00+00:00"
image = "/images/blog/2017-12-20-nintendo-ds/recalbox-nintendo-ds-banner.png"
title = "Nintendo DS on X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hi pals,

It is time to welcome a new emulated system on **X86/X86_64** Recalbox version: **the Nintendo DS !**   

So, we have added 2 new libretro cores to do this job. The [libretro desmume core](https://github.com/libretro/desmume) and the [libretro melonDS core](https://github.com/libretro/melonDS) based on a new Nintendo DS emulator.   
To emulate this new system you **must** have a good hardware. We have configured desmume core to work on most current setups, but if you don't have a fullspeed emulation, you can try to "downgrade" settings in retroarch menu (downgrade internal resolution, add a frameskip, etc...). In the other side, if you have a powerful setup, you can use this setting menu to boost graphics renderer.   

Unfortunately, this system is X86 only, because even on our strongest compatible ARM board, the Odroid XU4, emulation is not fullspeed even with the lowest settings + frameskip enabled.   
So we decided not to enable this system on other Recalbox versions.

For your information, **libretro melonDS core** needs to add some new bios. Here are the files to [add in your Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) :

**df692a80a5b1bc90728bc3dfc76cd948** - **bios7.bin**   
**a392174eb3e572fed6447e956bde4b25** - **bios9.bin**   
**e45033d9b0fa6b0de071292bba7c9d13** - **firmware.bin**   

We hope you'll enjoy these new cores and (re)discover the Nintendo DS games library.

## Different display modes (standard/hybrid)

Standard:   
[![display_standard](/images/blog/2017-12-20-nintendo-ds/display-standard-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-standard.png)

Hybrid:   
[![display_hybrid](/images/blog/2017-12-20-nintendo-ds/display-hybrid-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-hybrid.png)

## Retro Shader

[![shader_retro_mario](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario.png)
[![shader_retro_kart](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart.png)

## Scanlines Shader

[![shader_scanlines_mario](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario.png)
[![shader_scanlines_kart](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart.png)
