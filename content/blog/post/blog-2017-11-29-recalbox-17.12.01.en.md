+++
date = "2017-12-01T16:00:00+02:00"
image = "/images/blog/2017-11-29-recalbox-17-12-01/recalbox-17.12.01-banner.png"
title = "Recalbox 17.12.01"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++

Hi friends !

Today it is time to release this new **17.12.01** version of Recalbox !

Let's get in the depths of the new features :

* We have done a big emulators update. So retroarch gets bumped on **1.6.9**, ScummVM on **1.10.0** "unstable", DosBox on **0.74 r4063** (Thanks [lmerckx](https://gitlab.com/lmerckx)) and **all libretro cores** have been bumped on last version ! See more on this [dedicated blogpost]({{<ref "blog-2017-11-17-bump-emulators.en.md">}})   
_For your information, the **fba-libretro** core is now based on fba **v0.2.97.42**._   
_So don't forget to upgrade your romset before playing. The new **.dat** files are_ [available here](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro) !
* On X86 and X86_64 Recalbox version, you can now emulate PSX with the **mednafen_psx** cores ! See more on this [dedicated blogpost]({{<ref "blog-2017-11-11-x86-mednafen-psx.en.md">}})
* Dolphin emulator now supports hotkey shortcuts and you can access to the GUI settings. We also enhanced the **emulated wiimotes** support !
* On X86 and X86_64 we added a new software, **imlib2_grab** to be able to take command line screenshots !
* On X86 and X86_64 again, the boot menu (GRUB) have now a **verbose** option, to help you/us in support.
* We also fixed a bug about configuration of several wifi networks. Thanks [OyyoDams](https://gitlab.com/OyyoDams)
* From Odroid C2 & XU4 side, you can now play N64 with the mupen64plus **GLideN64 video plugin** (defined by default on XU4) !
* And some other points that you can read on the [changelog](https://gitlab.com/recalbox/recalbox/blob/master/CHANGELOG.md) displayed on your Recalbox !


Many new cool features are coming soon.    
So stay tuned and have fun with this new release ;)
