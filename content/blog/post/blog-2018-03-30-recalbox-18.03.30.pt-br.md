+++
date = "2018-03-30T01:00:00+02:00"
image = "/images/blog/2018-03-30-recalbox-18-03-30/recalbox-18.03.30-banner.jpg"
title = "Recalbox 18.03.30"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

E aí companheiros recalboxers!

Esta versão não traz funcionalidades novas, apenas correções da versão **18.03.16**. Eis a lista:

* **Moonlight: problemas na geração de GUID SDL2**. Graças ao criador do Moonlight este bug foi resolvido rapidamente!

* **Odroid-xu4: remoção do barulho agudo no arranque da ventoinha**. A voltagem é muito baixa para alimentar a nova geração de ventoinhas. Este fato impedia que a ventoinha iniciasse corretamente. Ela emitia um som irritante até que a temperatura fosse alta o suficiente para que a voltagem gerada fosse a adequada. Problema corrigido!

* **configgen: corrigido videoconfig=auto**. Vários de vocês tiveram problemas ao sair de emuladores (principalmente N64 e Dreamcast) se deparando com o ES esticado. Graças ao feedback da comunidade, isto foi resolvido.

* **ES: corrigida a lerdeza na transição do primeiro ao último sistema**. Agora está mais rápido do que nunca!

* **Tema: diversas correções ao recalbox-next**. Melhorias em problemas de legibilidade aqui e ali, e agora está perfeito!

* **manager: correção nas credenciais e menu**. A nova opção de segurança que exige a autenticação no gerenciador web está funcionando!

* **forçar a criação de arquivo uuid que estiver faltando** Caras, por favor não mexam em arquivos que vocês não sabem para que servem. Este é bastante útil e bloqueia atualizações caso esteja faltando.

Ainda há um bug em que o ES é finalizado quando o Recalbox está com muitas roms. O ES dá pau após diversos jogos serem lançados. Há um vazamento de memória (memory leak) que precisamos corrigir. Este é um tipo muito irritante de problema a ser debugado, podendo eventualmente levar algumas atualizações até ser corrigido. Enquanto isto, reinicie o ES com seu telefone através do gerenciador web!

E agora, algo relacionado ao **Pi3b+**: O Recalbox ainda não é compatível. Apesar de estarmos trabalhando do nosso lado (sem lançamentos publicos, não peça links), ele requer uma atualização completa de SO na qual estamos trabalhando nos últimos 3 meses. Não podemos lançar esta atualização ainda, já que poderá quebrar as suas instalações feitas no Pi e x86. Estamos criando um novo procedimento de atualização que parece bom segundo nossas avaliações. Como sempre, seja paciente. Vocês bem sabem que não somos rápidos para adaptar o Recalbox para novas placas Raspberry ;) Mas não será tão demorado quando a adição de suporte ao bluetooth interno do Pi3!

Finalmente: apesar deste lançamento ocorrer um pouco antes do Primeiro de Abril, decidimos não incluir o vídeo de "Feliz Ano Novo" na inicialização, para aqueles que sentiram falta do vídeo do homem de neve ;)
