+++
date = "2019-03-22T11:54:12Z"
image = "/images/blog/2019-03-22-upcoming-stable-release/banner.jpg"
title = "Lançamento estável a caminho"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Boas notícias!

Estamos a apenas alguns dias do próximo lançamento estável do Recalbox!

Mas antes, precisamos da sua ajuda!
Nos ajude a caçar os bugs críticos remanescentes que podemos ter deixado passar, antes da chegada da versão final.

## Esquema de versão

Como você deve ter notado, algumas versões atrás alteramos nosso esquema de versionamento: antes utilizavamos números (`4.0`, `4.1`, …) e trocamos para um esquema de versão numérica baseada em data (`18.04.20`, `18.06.27`, …) ao invés de criar a versão `5.x`.

Infelizmente versões baseadas em data são difíceis de ler, difíceis de escrever e difíceis de pronunciar. Em outras palavras: não são amigáveis.

Então, decidimos **reverter para o esquema de versão numérica tradicional** que é mais fácil de usar e tem uma melhor semântica (você reconhece o quão importante é uma versão baseada no seu número, enquanto que a versão baseada em datas dá pouca relevância para informações úteis).

Enfim, consideramos que todas aquelas versões baseadas em data representam a versão `5.x` e a próxima versão estável será a… `6.0` 🎉

E como um filho, provavelmente iremos batizar este projeto nas versões favoritas 😉 Nada foi decidido ainda, mas esta versão irá certamente receber um nome! ❤️

## Release Candidate 1

Nosso time de desenvolvimento passou os últimos meses adicionando ótimas funcionalidades, emuladores, atualizações, temas, sistemas, suporte a hardware, … Não entraremos em detalhes das novas e excitantes adições nesta postagem, contudo, daremos uma explicação detalhada das principais melhorias na postagem do próximo lançamento estável.

A quantidade de código novo adicionado exigiu que cada otimização fosse testada exaustivamente nos últimos meses para evitar problemas. Um dos principais fatores do tempo levado para este lançamento: testamos extensivamente para garantir uma experiência de jogos melhor possível.

E isto foi possível graças a vocês!

Com a ajuda desta forte comunidade (auxiliada pelo time de suporte igualmente maneiro!), pudemos lançar 3 versões _beta_ diferentes que resultaram em uma penca de novos bugs que foram corrigidos pelos nossos desenvolvedores o mais rápido possível (lembra como todo o trabalho do time é voluntário?).

Pois bem, foi um longo caminho até o lançamento desta nova versão do Recalbox!

Estamos tão confiantes e orgulhosos do nosso trabalho que consideramos que um lançamento poderia ser feito agora mesmo, da forma como está hoje.

Mas isto não é o bastante! (ainda) Queremos que este próximo lançamento estável seja examentente isto: estável 💪

Por isto **estamos lançando um Release Candidate (RC)**. Como diz o nome, um Release Candidate é uma versão de software boa o bastante para ser promovia a software versão final. Estamos publicando esta versão alguns dias antes da final para que as pessoas possam testar numa variedade maior de hardware, configurações, _bios_ e _roms_ para que possamos pegar bugs críticos que podemos ter deixado passar 😬

E como vocês sabem, versões _beta_ tem como foco usuários avançados e que conseguem encontrar, reproduzir e reportar bugs (com detalhes e logs). Já um Release Candidate, deve ser usado supostamente por usuários "medianos" mas que conseguem até um certo grau reproduzir e reportar um bug (o que já nos ajuda bastante!). Não deve ser usado por quem não sabe aplicar uma imagem num cartão SD, não consegue testar alguns jogos ou configurar um controle.

Caso deseje ajudar **o que esperamos é que você** [baixe a imagem](https://recalbox-releases.s3.nl-ams.scw.cloud/stable/index.html) disponível para o seu hardware, configure suas preferências (opções, jogos, temas, …) ou até mesmo utilize as configurações do passado e nos informe se algo quebrar 💥

Se você encontrar algum problema grave, entre em contato conosco no [Discord](https://discord.gg/d2xCQ4e) com todos os detalhes abaixo para que possamos entender e consertar o problema de forma eficiente:

* Versão do Recalbox (já que pode haver mais que um RC)
* seu hardware: placa e controles
* tema utilizado
* alterações com relação a configuração padrão
* passos para reproduzir o bug
* comportamento esperado
* comportamento real

Algumas coisas estão fora do escopo deste RC e não devem ser reportadas (e se forem, provavelmente não serão consertadas antes do lançamento estável): 

* problemas específicos de controles
* problemas específicos de _roms_
* problemas específicos de temas
* problemas conhecidos (e que serão resolvidos depois do lançamento final):
  * trapaças que não funcionam no Mame2010
  * proporção de tela do Retroarch que não condiz com o ES/Configgen
  * Kodi não inicia corretamente com alguns controles

# Atualizações (não!)

Se você já estiver utilizando a última versão estável do Recalbox, ou uma das versões _beta_ e deseja atualizar para este RC através do procedimento de atualização interna do sistema: **não faça!**

Mudamos significativamente o processo de atualização do Recalbox, incompatibilizando versões antigas com a infraestrutura atual. Infelizmente não há uma forma de atualizar das versões anteriores para esta.

Atualizações deste RC para um próximo RC ou até mesmo para a versão estável deverão funcionar.

Por favor, **execute uma nova instalação**, teste, jogue, mexa, divirta-se… e nos mantenha informados 🤗
