+++
date = "2017-12-20T10:00:00+00:00"
image = "/images/blog/2017-12-20-nintendo-ds/recalbox-nintendo-ds-banner.png"
title = "Nintendo DS para X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá amigos,

É hora de dar boas vindas a um novo sistema emulado no Recalbox versão **X86/X86_64**: **o Nintendo DS !**

Nós adicionamos 2 novos cores libretro para cumprir esta função. O [core libretro desmume](https://github.com/libretro/desmume) e o [core libretro melonDS](https://github.com/libretro/melonDS) que é baseado em um novo emulador de Nintendo DS.
Para emular este novo sistema você **deve** possuir um bom hardware. Nós configuramos o core desmume para funcionar com setups atuais, mas caso você não consiga uma emulação em velocidade total, você pode fazer um "downgrade" de configurações no menu do retroarch (downgrade de resolução interna, adicionar frameskip, etc...). Caso contrário, com um setup poderoso, você poderá usar configurações para incrementar a renderização gráfica. 

Infelizmente este sistema está apenas disponível em X86, porque até mesmo na nossa placa ARM compatível mais forte, a Odroid XU4, a emulação não roda em sua velocidade total mesmo com as configurações mais baixas + frameskip habilitado.
Portanto, decidimos não habilitar este sistema em outras versões do Recalbox.

Para sua informação, o **core libretro melonDS** precisa de algumas novas bios. Estes são os arquivos a serem [adicionados ao seu Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) :

**df692a80a5b1bc90728bc3dfc76cd948** - **bios7.bin**   
**a392174eb3e572fed6447e956bde4b25** - **bios9.bin**   
**e45033d9b0fa6b0de071292bba7c9d13** - **firmware.bin**   

Esperamos que vocês curtam estes novos cores e (re)descubram a biblioteca de jogos do Nintendo DS.

## Diferentes disposições dos displays (standard/hybrid)

Standard:   
[![display_standard](/images/blog/2017-12-20-nintendo-ds/display-standard-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-standard.png)

Hybrid:   
[![display_hybrid](/images/blog/2017-12-20-nintendo-ds/display-hybrid-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-hybrid.png)

## Retro Shader

[![shader_retro_mario](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario.png)
[![shader_retro_kart](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart.png)

## Scanlines Shader

[![shader_scanlines_mario](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario.png)
[![shader_scanlines_kart](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart.png)
